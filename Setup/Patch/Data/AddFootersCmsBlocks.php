<?php
namespace Beside\Install\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Cms\Model\BlockFactory;
use Magento\Cms\Model\ResourceModel\Block\CollectionFactory;


/**
 * Class AddFootersCmsBlocks
 *
 * @package Beside\Setup\Setup\Patch\Data
 */
class AddFootersCmsBlocks implements DataPatchInterface
{
    const ENGLISH_STORE_ID = [2, 4];
    const ARABIC_STORE_ID = [3, 5];


    /**
     * @var \Magento\Cms\Model\ResourceModel\Block
     */
    private $blockResource;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * File instance
     *
     * @var \Magento\Framework\Filesystem\Driver\File
     *
     */
    private $file;

    public function __construct(
        \Magento\Cms\Model\BlockFactory $blockFactory,
        \Magento\Cms\Model\ResourceModel\Block $blockResource,
        \Magento\Framework\Filesystem\Driver\File $file
    ) {
        $this->blockFactory = $blockFactory;
        $this->blockResource = $blockResource;
        $this->file = $file;
    }

    public function apply()
    {
        $cmsBlockData = [
            [
                'identifier' => 'footer-social-media-en',
                'title'      => 'Footer Social media (en)',
                'content'    => 'footer-social-media-en.html',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'footer-social-media-ar',
                'title'      => 'Footer Social media (ar)',
                'content'    => 'footer-social-media-ar.html',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ],
            [
                'identifier' => 'footer-links-collapsible_en',
                'title'      => 'Footer Links Collapsible (EN)',
                'content'    => 'footer-links-collapsible_en.html',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'footer-links-collapsible_ar',
                'title'      => 'Footer Links Collapsible (AR)',
                'content'    => 'footer-links-collapsible_ar.html',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ]
        ];

        $baseDir = __DIR__
            . DIRECTORY_SEPARATOR . 'data'
            . DIRECTORY_SEPARATOR . 'cms'
            . DIRECTORY_SEPARATOR . 'block'
            . DIRECTORY_SEPARATOR;

        foreach ($cmsBlockData as $data) {
            $content = $this->file->fileGetContents($baseDir . $data['content']);
            $block = $this->blockFactory->create();
            $this->blockResource->load($block, $data['identifier'], 'identifier');
            $block->setStoreId($data['stores']);
            $block->setIdentifier($data['identifier']);
            $block->setTitle($data['title']);
            $block->setContent($content);
            $this->blockResource->save($block);
        }
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function revert()
    {
        return [];
    }
}
