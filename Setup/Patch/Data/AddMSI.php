<?php
namespace Beside\Install\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddMSI implements DataPatchInterface
{

    /**
     * @var StockRepositoryInterface
     */
    protected $stockRepo;

    /**
     * @var StockInterfaceFactory
     */
    protected $stock;

    protected $source;
    protected $sourceRepo;

    /**
     * DefaultCustomerGroupsAndAttributes constructor.
     * @param StockRepositoryInterface $stockRepo
     * @param StockInterfaceFactory $stock
     */
    public function __construct(
        \Magento\InventoryApi\Api\StockRepositoryInterface $stockRepo,
        \Magento\InventoryApi\Api\Data\StockInterfaceFactory $stock,
        \Magento\InventoryApi\Api\Data\SourceInterfaceFactory $source,
        \Magento\InventoryApi\Api\SourceRepositoryInterface $sourceRepo
    ) {
        $this->stockRepo = $stockRepo;
        $this->stock = $stock;
        $this->source = $source;
        $this->sourceRepo = $sourceRepo;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function apply()
    {
        $stockItem = $this->stock->create();
        $stockItem->setStockId(123);
        $stockItem->setName("testtest");

       // $sourceItem->setStatus(1);

        $this->stockRepo->save($stockItem);

        $sourceInstance = $this->source->create();

        $sourceInstance->setSourceCode("default");
        $sourceInstance->setName("KSA Source");
       // $sourceInstance->getCity("cape town");
      //  $sourceInstance->getLatitude(123445);
      //  $sourceInstance->getLongitude(234555);
        $sourceInstance->setEnabled(1);
        $sourceInstance->setPostcode(7320);
        $sourceInstance->setCountryId('SA');

        $this->sourceRepo->save($sourceInstance);

        $sourceInstance = $this->source->create();

        $sourceInstance->setSourceCode("uae_source");
        $sourceInstance->setName("UAE Source");
        // $sourceInstance->getCity("cape town");
        //  $sourceInstance->getLatitude(123445);
        //  $sourceInstance->getLongitude(234555);
        $sourceInstance->setEnabled(1);
        $sourceInstance->setPostcode(7320);
        $sourceInstance->setCountryId('AE');

        $this->sourceRepo->save($sourceInstance);
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
