<?php
namespace Beside\Install\Setup\Patch\Data;

use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Catalog\Model\ResourceModel\Attribute as ResourceModel;

class AddCustomProductVarcharAttributes implements DataPatchInterface
{
    /**
     * @var array
     */
    private $attributes = [
        ['code'=> 'item_number', 'label' => 'Item Number', 'visible_on_front' => 0],
        ['code'=> 'brand', 'label' => 'Brand', 'visible_on_front' => 1],
        ['code'=> 'micro_colour', 'label' => 'Micro Colour', 'visible_on_front' => 1],
        ['code'=> 'composition', 'label' => 'Composition', 'visible_on_front' => 1],
        ['code'=> 'model_image_size', 'label' => 'Model Image Size', 'visible_on_front' => 1],
        ['code'=> 'measurements', 'label' => 'Measurements', 'visible_on_front' => 1],
        ['code'=> 'fitting_info', 'label' => 'Fitting Info', 'visible_on_front' => 1],
        ['code'=> 'collar', 'label' => 'Collar', 'visible_on_front' => 1],
        ['code'=> 'length', 'label' => 'Length', 'visible_on_front' => 1],
        ['code'=> 'shape', 'label' => 'Shape', 'visible_on_front' => 1],
        ['code'=> 'sleeves', 'label' => 'Sleeves', 'visible_on_front' => 1],
        ['code'=> 'plp_description', 'label' => 'PLP Description', 'visible_on_front' => 1],
        ['code'=> 'washing', 'label' => 'Washing', 'visible_on_front' => 1],
        ['code'=> 'bleaching', 'label' => 'Bleaching', 'visible_on_front' => 1],
        ['code'=> 'ironing', 'label' => 'Ironing', 'visible_on_front' => 1],
        ['code'=> 'dry_cleaning', 'label' => 'Dry Cleaning', 'visible_on_front' => 1],
        ['code'=> 'machine_drying', 'label' => 'Machine Drying', 'visible_on_front' => 1],
        ['code'=> 'attribute_set', 'label' => 'Attribute Set', 'visible_on_front' => 0],
        ['code'=> 'main_group_custom', 'label' => 'Main group', 'visible_on_front' => 0],
        ['code'=> 'group_cistom', 'label' => 'Group', 'visible_on_front' => 0],
        ['code'=> 'micro_category', 'label' => 'Micro Category', 'visible_on_front' => 0],
        ['code'=> 'datural_drying', 'label' => 'Natural Drying', 'visible_on_front' => 1]
    ];

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * @var EavConfig
     */
    private $eavConfig;

    /**
     * AddCustomProductVarcharAttributes constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     * @param EavConfig $eavConfig
     * @param AttributeSetFactory $attributeSetFactory
     * @param ResourceModel $resourceModel
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        EavConfig $eavConfig,
        AttributeSetFactory $attributeSetFactory,
        ResourceModel $resourceModel
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->resourceModel = $resourceModel;
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        $productEntity = $this->eavConfig->getEntityType(Product::ENTITY);
        $attributeSetId = $productEntity->getDefaultAttributeSetId();

        foreach ($this->attributes as $attribute) {
            /** @var EavSetup $eavSetup */
            $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

            $eavSetup->addAttribute(
                Product::ENTITY,
                $attribute['code'],
                [
                    'type' => 'varchar',
                    'label' => $attribute['label'],
                    'input' => 'text',
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => $attribute['visible_on_front'],
                    'unique' => false,
                ]
            );

            /** @var \Magento\Eav\Model\Entity\Attribute\AbstractAttribute $createdAttribute */
            $createdAttribute = $this->eavConfig->getAttribute(
                Product::ENTITY,
                $attribute['code']
            );

            /** @var $attributeSet AttributeSet */
            $attributeSet = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

            $data = [
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId
            ];

            $createdAttribute->addData($data);
            $this->resourceModel->save($createdAttribute);
        }
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function revert()
    {
        return [];
    }
}
