<?php
namespace Beside\Install\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Cms\Model\BlockFactory;
use Magento\Cms\Model\ResourceModel\Block\CollectionFactory;


/**
 * Class AddAccountDashboardCmsBlocks
 *
 * @package Beside\Setup\Setup\Patch\Data
 */
class AddAccountDashboardCmsBlocks implements DataPatchInterface
{
    const ENGLISH_STORE_ID = [2, 4];
    const ARABIC_STORE_ID = [3, 5];


    /**
     * @var \Magento\Cms\Model\ResourceModel\Block
     */
    private $blockResource;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * File instance
     *
     * @var \Magento\Framework\Filesystem\Driver\File
     *
     */
    private $file;

    public function __construct(
        \Magento\Cms\Model\BlockFactory $blockFactory,
        \Magento\Cms\Model\ResourceModel\Block $blockResource,
        \Magento\Framework\Filesystem\Driver\File $file
    ) {
        $this->blockFactory = $blockFactory;
        $this->blockResource = $blockResource;
        $this->file = $file;
    }

    public function apply()
    {
        $cmsBlockData = [
            [
                'identifier' => 'customer_promotions_en',
                'title'      => 'Customer Promotions (EN)',
                'content'    => 'customer_promotions_en.html',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'customer_promotions_ar',
                'title'      => 'Customer Promotions (AR)',
                'content'    => 'customer_promotions_ar.html',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ],
            [
                'identifier' => 'customer_promotions_online_popup_en',
                'title'      => 'Customer Promotions Online Popup (EN)',
                'content'    => 'customer_promotions_online_popup_en.html',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'customer_promotions_online_popup_ar',
                'title'      => 'Customer Promotions Online Popup (AR)',
                'content'    => 'customer_promotions_online_popup_ar.html',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ],
            [
                'identifier' => 'customer_promotions_store_popup_en',
                'title'      => 'Customer Promotions Store Popup (EN)',
                'content'    => 'customer_promotions_store_popup_en.html',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'customer_promotions_store_popup_ar',
                'title'      => 'Customer Promotions Store Popup (AR)',
                'content'    => 'customer_promotions_store_popup_ar.html',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ],
            [
                'identifier' => 'account_benefits_block_en',
                'title'      => 'Account Benefits Block (EN)',
                'content'    => 'account_benefits_block_en.html',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'account_benefits_block_ar',
                'title'      => 'Account Benefits Block (AR)',
                'content'    => 'account_benefits_block_ar.html',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ],
            [
                'identifier' => 'account_help_block_en',
                'title'      => 'Account Benefits Block (EN)',
                'content'    => 'account_help_block_en.html',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'account_help_block_ar',
                'title'      => 'Accout help block (AR)',
                'content'    => 'account_help_block_ar.html',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ]

        ];

        $baseDir = __DIR__
            . DIRECTORY_SEPARATOR . 'data'
            . DIRECTORY_SEPARATOR . 'cms'
            . DIRECTORY_SEPARATOR . 'block'
            . DIRECTORY_SEPARATOR;

        foreach ($cmsBlockData as $data) {
            $content = $this->file->fileGetContents($baseDir . $data['content']);
            $block = $this->blockFactory->create();
            $this->blockResource->load($block, $data['identifier'], 'identifier');
            $block->setStoreId($data['stores']);
            $block->setIdentifier($data['identifier']);
            $block->setTitle($data['title']);
            $block->setContent($content);
            $this->blockResource->save($block);
        }
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function revert()
    {
        return [];
    }
}
