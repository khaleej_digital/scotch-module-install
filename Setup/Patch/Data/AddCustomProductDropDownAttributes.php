<?php
namespace Beside\Install\Setup\Patch\Data;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Catalog\Model\ResourceModel\Attribute as ResourceModel;
use Magento\Eav\Model\Config;

class AddCustomProductDropDownAttributes implements DataPatchInterface
{
    /**
     * @var array
     */
    private $selectAttribute = [
        [
            'code'=> 'size',
            'label' => 'Size',
            'visible_on_front' => 1,
            'option' => ['values' => [
                'S', 'L', 'M', '01', '02', '100', '105', '10Y', '12M', '12Y', '14Y', '16Y', '18M', '24', '24M', '25',
                '26', '27', '28', '29', '30','31', '32', '33', '34', '35', '36', '36M', '37', '38', '39', '3M', '3XL',
                '40', '41', '42', '43', '44', '45', '4Y', '6M', '6Y', '80', '85', '80Y', '90', '95', '9M', 'I', 'II',
                'III', 'IV', 'XL', 'XS', 'XXL', 'XXS', 'UNI', '1', '2', '8Y'
            ]]
        ],
        [
            'code'=> 'gender',
            'label' => 'Gender',
            'visible_on_front' => 0,
            'option' => ['values' => ['Woman', 'Man']]
        ],
        [
            'code'=> 'macro_colour',
            'label' => 'Macro Colour',
            'visible_on_front' => 1,
            'option' => [
                'values' => ['Multicolor', 'Black', 'Orange', 'Silver', 'Pink', 'Blue', 'Violet', 'Yellow', 'Green', 'Brown', 'Grey', 'Red', 'White']
            ]
        ],
        [
            'code'=> 'main_category',
            'label' => 'Main Category',
            'visible_on_front' => 1,
            'option' => ['values' => [
                'Accessories', 'Bags', 'Beachwear', 'Belts', 'Dresses', 'Jackets', 'Jumpsuits', 'Knitwear', 'Lungewear',
                'Polos', 'Shoes', 'Skirts', 'Socks', 'Shirts', 'Underwear', 'Wallets', 'Sweaters', 'T-shirts',
                'T-shirts and Tops', 'Trousers and Shorts', 'Loungewear'
            ]]
        ],
        [
            'code'=> 'material',
            'label' => 'Material',
            'visible_on_front' => 1,
            'option' => ['values' => ['Cotton', 'Denim', 'Leather', 'Lyocell', 'Other fabrics/mixed materials', 'Viscose', 'Wool', 'Nylon']]
        ],
        [
            'code'=> 'print',
            'label' => 'Print',
            'visible_on_front' => 1,
            'option' => ['values' => ['Allover', 'Check', 'Graphic', 'Solid', 'Other', 'Stripes', 'Logo']]
        ]
    ];

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * @var ResourceModel
     */
    private $resourceModel;

    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * AddCustomProductAttributesT constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     * @param ResourceModel $resourceModel
     * @param Config $eavConfig
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        AttributeSetFactory $attributeSetFactory,
        ResourceModel $resourceModel,
        Config $eavConfig
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->resourceModel = $resourceModel;
        $this->eavConfig = $eavConfig;
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $productEntity = $this->eavConfig->getEntityType(Product::ENTITY);

        foreach ($this->selectAttribute as $attribute) {
            $eavSetup->addAttribute(
                Product::ENTITY,
                $attribute['code'],
                [
                    'type' => 'int',
                    'label' => $attribute['label'],
                    'input' => 'select',
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'default' => null,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => $attribute['visible_on_front'],
                    'used_in_product_listing' => true,
                    'option' => $attribute['option']
                ]
            );

            $createdAttribute = $this->eavConfig->getAttribute(
                Product::ENTITY,
                $attribute['code']
            );

            $attributeSetId = $productEntity->getDefaultAttributeSetId();
            /** @var $attributeSet AttributeSet */
            $attributeSet = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

            $data = [
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId
            ];

            $createdAttribute->addData($data);
            $this->resourceModel->save($createdAttribute);
        }
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function revert()
    {
        return [];
    }
}
