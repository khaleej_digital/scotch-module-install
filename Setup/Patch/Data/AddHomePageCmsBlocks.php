<?php
namespace Beside\Install\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Cms\Model\BlockFactory;
use Magento\Cms\Model\ResourceModel\Block\CollectionFactory;


/**
 * Class AddHomePageCmsBlocks
 *
 * @package Beside\Setup\Setup\Patch\Data
 */
class AddHomePageCmsBlocks implements DataPatchInterface
{
    const ENGLISH_STORE_ID = [2, 4];
    const ARABIC_STORE_ID = [3, 5];


    /**
     * @var \Magento\Cms\Model\ResourceModel\Block
     */
    private $blockResource;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * File instance
     *
     * @var \Magento\Framework\Filesystem\Driver\File
     *
     */
    private $file;

    public function __construct(
        \Magento\Cms\Model\BlockFactory $blockFactory,
        \Magento\Cms\Model\ResourceModel\Block $blockResource,
        \Magento\Framework\Filesystem\Driver\File $file
    ) {
        $this->blockFactory = $blockFactory;
        $this->blockResource = $blockResource;
        $this->file = $file;
    }

    public function apply()
    {
        $cmsBlockData = [
            [
                'identifier' => 'hp-top-banner_en',
                'title'      => 'HomePage Top Banner (en)',
                'content'    => 'hp-top-banner_en.phtml',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'hp-top-banner_ar',
                'title'      => 'HomePage Top Banner (ar)',
                'content'    => 'hp-top-banner_ar.phtml',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ],

            [
                'identifier' => 'hp-top-image-banner_en',
                'title'      => 'HP Top Image Banner (EN)',
                'content'    => 'hp-top-image-banner_en.phtml',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'hp-top-image-banner_ar',
                'title'      => 'HP Top Image Banner (ar)',
                'content'    => 'hp-top-image-banner_ar.phtml',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ],

            [
                'identifier' => 'hp-two-image-blocks_en',
                'title'      => 'HomePage two image blocks (en)',
                'content'    => 'hp-two-image-blocks_en.phtml',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'hp-two-image-blocks_ar',
                'title'      => 'HomePage two image blocks (ar)',
                'content'    => 'hp-two-image-blocks_ar.phtml',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ],

            [
                'identifier' => 'hp-four-image-blocks_en',
                'title'      => 'HomePage four image blocks (en)',
                'content'    => 'hp-four-image-blocks_en.phtml',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'hp-four-image-blocks_ar',
                'title'      => 'HomePage four image blocks (ar)',
                'content'    => 'hp-four-image-blocks_ar.phtml',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ],

            [
                'identifier' => 'hp-news-bloc_en',
                'title'      => 'HomePage News Block (en)',
                'content'    => 'hp-news-bloc_en.phtml',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'hp-news-bloc_ar',
                'title'      => 'HomePage News Block (ar)',
                'content'    => 'hp-news-bloc_ar.phtml',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ],

            [
                'identifier' => 'hp-three-image-blocks_en',
                'title'      => 'HomePage three image blocks (en)',
                'content'    => 'hp-three-image-blocks_en.phtml',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'hp-three-image-blocks_ar',
                'title'      => 'HomePage three image blocks (ar)',
                'content'    => 'hp-three-image-blocks_ar.phtml',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ],

            [
                'identifier' => 'hp-one-image-blocks_en',
                'title'      => 'HomePage one image blocks (en)',
                'content'    => 'hp-one-image-blocks_en.phtml',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'hp-one-image-blocks_ar',
                'title'      => 'HomePage one image blocks (ar)',
                'content'    => 'hp-one-image-blocks_ar.phtml',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ],

            [
                'identifier' => 'hp-usp-block_en',
                'title'      => 'HomePage USP Block (en)',
                'content'    => 'hp-usp-block_en.phtml',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'hp-usp-block_ar',
                'title'      => 'HomePage USP Block (ar)',
                'content'    => 'hp-usp-block_ar.phtml',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ],

        ];

        $baseDir = __DIR__
            . DIRECTORY_SEPARATOR . 'data'
            . DIRECTORY_SEPARATOR . 'cms'
            . DIRECTORY_SEPARATOR . 'block'
            . DIRECTORY_SEPARATOR;

        foreach ($cmsBlockData as $data) {
            $content = $this->file->fileGetContents($baseDir . $data['content']);
            $block = $this->blockFactory->create();
            $this->blockResource->load($block, $data['identifier'], 'identifier');
            $block->setStoreId($data['stores']);
            $block->setIdentifier($data['identifier']);
            $block->setTitle($data['title']);
            $block->setContent($content);
            $this->blockResource->save($block);
        }
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function revert()
    {
        return [];
    }
}
