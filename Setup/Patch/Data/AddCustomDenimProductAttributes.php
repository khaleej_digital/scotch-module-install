<?php
namespace Beside\Install\Setup\Patch\Data;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Catalog\Model\ResourceModel\Attribute as ResourceModel;
use Magento\Eav\Model\Config;

class AddCustomDenimProductAttributes implements DataPatchInterface
{
    /**
     * @var array
     */
    private $denimAttribute = [
        [
            'code' => 'look',
            'type' => 'int',
            'input' => 'select',
            'label' => 'Look',
            'visible_on_front' => 1,
            'option' => ['values' => ['Tapered', 'Skinny', 'Boyfriend', 'Carrot',  'Straight', 'Slim', 'Super skinny', 'Bootcut and Flare']]
        ],
        [
            'code' => 'wash_type',
            'type' => 'int',
            'input' => 'select',
            'label' => 'Wash Type',
            'visible_on_front' => 1,
            'option' => ['values' => ['Clean', 'Remastered', 'Treated',  'Straight', 'Slim', 'Super skinny', 'Bootcut and Flare']]
        ],
        [
            'code' => 'denim_length',
            'type' => 'int',
            'input' => 'select',
            'label' => 'Denum Length',
            'visible_on_front' => 1,
            'option' => ['values' => ['30', '32', '34']]
        ],
        [
            'code' => 'fabric_type',
            'type' => 'int',
            'input' => 'select',
            'label' => 'Fabric Type',
            'visible_on_front' => 1,
            'option' => ['values' => ['JoggJeans', 'Non-Stretch', 'Stretch', 'Super stretch']]
        ],
        [
            'code' => 'waist_rise',
            'type' => 'int',
            'input' => 'select',
            'label' => 'Waist Rise',
            'visible_on_front' => 1,
            'option' => ['values' => ['High waist', 'Low waist', 'Regular waist', 'Super stretch']]
        ],
        [
            'code'=> 'style_group',
            'type' => 'int',
            'input' => 'select',
            'label' => 'Style Group',
            'visible_on_front' => 1,
            'option' => ['values' => [
                'Babhila', 'D-Amny', 'D-Azzer JoggJeans®', 'D-Concias', 'D-Ebbey', 'D-Fining', 'D-Istort', 'D-Jevel', 'D-Joy',
                'D-Kras', 'D-Luster', 'D-Macs', 'D-Mihtry', 'D-REEFT JoggJeans®', 'D-Reggy', 'D-Rifty', 'D-Roisin',
                'D-Roisin High', 'D-Spritzz', 'D-Strukt', 'D-Vider', 'Fayza', 'KROOLEY JoggJeans®', 'Slandy', 'Slandy Low',
                'Slandy-B', 'Sleenker', 'Thommer JoggJeans®', 'Slandy High'
            ]]
        ],
        ['code'=> 'wash_type_group', 'label' => 'Wash Type Group', 'visible_on_front' => 1, 'type' => 'varchar','input' => 'text'],
        ['code'=> 'wash_description', 'label' => 'Wash Description', 'visible_on_front' => 1, 'type' => 'text','input' => 'text'],
        ['code'=> 'style', 'label' => 'Style', 'visible_on_front' => 1, 'type' => 'varchar','input' => 'text'],
        ['code'=> 'style_editorial', 'label' => 'Style Editorial', 'visible_on_front' => 1, 'type' => 'varchar','input' => 'text'],
    ];

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;

    /**
     * @var ResourceModel
     */
    private $resourceModel;

    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * AddCustomProductAttributesT constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     * @param ResourceModel $resourceModel
     * @param Config $eavConfig
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        AttributeSetFactory $attributeSetFactory,
        ResourceModel $resourceModel,
        Config $eavConfig
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->resourceModel = $resourceModel;
        $this->eavConfig = $eavConfig;
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $productEntity = $this->eavConfig->getEntityType(Product::ENTITY);
        /** @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection $attributeSetCollection */
        $attributeSetCollection = $productEntity->getAttributeSetCollection()
            ->addFilter('attribute_set_name', 'denim');
        $attributeSetId = null;
        $attributeGroupId = null;

        foreach ($attributeSetCollection->getItems() as $item) {
            $attributeSetId = $item->getData('attribute_set_id');
            /** @var $attributeSet AttributeSet */
            $attributeSet = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
        }

        foreach ($this->denimAttribute as $attribute) {
            $eavSetup->addAttribute(
                Product::ENTITY,
                $attribute['code'],
                $this->prepareArtibuteData($attribute)
            );

            if ($attributeSetId && $attributeGroupId) {
                $createdAttribute = $this->eavConfig->getAttribute(
                    Product::ENTITY,
                    $attribute['code']
                );

                $data = [
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId
                ];

                $createdAttribute->addData($data);
                $this->resourceModel->save($createdAttribute);
            }
        }
    }

    /**
     * @param array $attribute
     * @return array
     */
    private function prepareArtibuteData(array $attribute)
    {
        $attributeData = [
            'type' => $attribute['type'],
            'label' => $attribute['label'],
            'input' => $attribute['input'],
            'visible' => true,
            'required' => false,
            'user_defined' => true,
            'default' => null,
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible_on_front' => $attribute['visible_on_front'],
        ];

        if (isset($attribute['option'])) {
            $attributeData['used_in_product_listing'] = true;
            $attributeData['option'] = $attribute['option'];
        }

        return $attributeData;
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function revert()
    {
        return [];
    }
}
