<?php
namespace Beside\Install\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Tax\Api\TaxRateRepositoryInterface;
use Magento\Tax\Api\Data\TaxRateInterfaceFactory;
use Magento\Tax\Api\TaxRuleRepositoryInterface;
use Magento\Tax\Api\Data\TaxRuleInterfaceFactory;
use Magento\Tax\Helper\Data as TaxHelperData;

/**
 * Class AddTaxRateRule
 *
 * @package Beside\Setup\Setup\Patch\Data
 */
class AddTaxRateRule implements DataPatchInterface, PatchRevertableInterface
{
    /**
     * Tax configurations for stores
     */
    private const STORES_TAX = [
        'uae' => [
            'rate_code' => 'UAE-tax-rate',
            'rule_code' => 'UAE-tax-rule',
            'rate_value' => '5.0000',
            'country_id' => 'AE',
            'rule_priority' => 0,
            'rule_position' => 0
        ],
        'ksa' => [
            'rate_code' => 'KSA-tax-rate',
            'rule_code' => 'KSA-tax-rule',
            'rate_value' => '15.0000',
            'country_id' => 'SA',
            'rule_priority' => 0,
            'rule_position' => 5
        ]
    ];

    /**
     * @var TaxRateRepositoryInterface
     */
    private TaxRateRepositoryInterface $taxRateRepository;

    /**
     * @var TaxRateInterfaceFactory
     */
    private TaxRateInterfaceFactory $taxRateFactory;

    /**
     * @var TaxRuleRepositoryInterface
     */
    private TaxRuleRepositoryInterface $taxRuleRepository;

    /**
     * @var TaxRuleInterfaceFactory
     */
    private TaxRuleInterfaceFactory $taxRuleFactory;

    /**
     * @var TaxHelperData
     */
    private TaxHelperData $taxHelperData;

    /**
     * AddTaxRateRule constructor.
     *
     * @param TaxRateRepositoryInterface $taxRateRepository
     * @param TaxRateInterfaceFactory $taxRateFactory
     * @param TaxRuleRepositoryInterface $taxRuleRepository
     * @param TaxRuleInterfaceFactory $taxRuleFactory
     * @param TaxHelperData $taxHelperData
     */
    public function __construct(
        TaxRateRepositoryInterface $taxRateRepository,
        TaxRateInterfaceFactory $taxRateFactory,
        TaxRuleRepositoryInterface $taxRuleRepository,
        TaxRuleInterfaceFactory $taxRuleFactory,
        TaxHelperData $taxHelperData
    ) {
        $this->taxRateRepository = $taxRateRepository;
        $this->taxRateFactory = $taxRateFactory;
        $this->taxRuleRepository = $taxRuleRepository;
        $this->taxRuleFactory = $taxRuleFactory;
        $this->taxHelperData = $taxHelperData;
    }

    /**
     * Create Tax Rates and Rules
     */
    public function apply()
    {
        $productTaxClassId = $this->taxHelperData->getDefaultProductTaxClass();
        $customerTaxClassId = $this->taxHelperData->getDefaultCustomerTaxClass();

        foreach (self::STORES_TAX as $tax) {
            // Tax Rate
            $taxRate = $this->taxRateFactory->create()
                ->setCode($tax['rate_code'])
                ->setRate($tax['rate_value'])
                ->setTaxCountryId($tax['country_id'])
                ->setTaxRegionId('')
                ->setZipFrom('')
                ->setZipIsRange('')
                ->setZipFrom('')
                ->setZipTo('')
                ->setTaxPostcode('*');
            $resultTaxRate = $this->taxRateRepository->save($taxRate);
            // Tax Rule
            if ($resultTaxRate) {
                $taxRule = $this->taxRuleFactory->create()
                    ->setCode($tax['rule_code'])
                    ->setTaxRateIds([$resultTaxRate->getId()])
                    ->setPriority($tax['rule_priority'])
                    ->setPosition($tax['rule_position'])
                    ->setProductTaxClassIds([$productTaxClassId])
                    ->setCustomerTaxClassIds([$customerTaxClassId]);
                $this->taxRuleRepository->save($taxRule);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function revert()
    {
        return [];
    }
}
