<?php
namespace Beside\Install\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Cms\Model\PageFactory;
use Magento\Cms\Model\ResourceModel\Page\CollectionFactory;


/**
 * Class AddHomePageContent
 *
 * @package Beside\Setup\Setup\Patch\Data
 */
class AddHomePageContent implements DataPatchInterface
{
    const ENGLISH_STORE_ID = [2, 4];
    const ARABIC_STORE_ID = [3, 5];


    /**
     * @var \Magento\Cms\Model\ResourceModel\Page
     */
    private $pageResource;

    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * File instance
     *
     * @var \Magento\Framework\Filesystem\Driver\File
     *
     */
    private $file;

    public function __construct(
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Magento\Cms\Model\ResourceModel\Page $pageResource,
        \Magento\Framework\Filesystem\Driver\File $file
    ) {
        $this->pageFactory = $pageFactory;
        $this->pageResource = $pageResource;
        $this->file = $file;
    }

    public function apply()
    {
        $cmsPageData = [
            [
                'identifier' => 'home_en',
                'title'      => 'Home page en',
                'content'    => 'home-page_en.phtml',
                'is_active'  => 1,
                'stores'     => self::ENGLISH_STORE_ID
            ],
            [
                'identifier' => 'home_ar',
                'title'      => 'Home page ar',
                'content'    => 'home-page_ar.phtml',
                'is_active'  => 1,
                'stores'     => self::ARABIC_STORE_ID
            ]

        ];

        $baseDir = __DIR__
            . DIRECTORY_SEPARATOR . 'data'
            . DIRECTORY_SEPARATOR . 'cms'
            . DIRECTORY_SEPARATOR . 'page'
            . DIRECTORY_SEPARATOR;

        foreach ($cmsPageData as $data) {
            $content = $this->file->fileGetContents($baseDir . $data['content']);
            $page = $this->pageFactory->create();
            $this->pageResource->load($page, $data['identifier'], 'identifier');
            $page->setStoreId($data['stores']);
            $page->setIdentifier($data['identifier']);
            $page->setPageLayout('1column');
            $page->setTitle($data['title']);
            $page->setContent($content);
            $this->pageResource->save($page);
        }
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function revert()
    {
        return [];
    }
}
